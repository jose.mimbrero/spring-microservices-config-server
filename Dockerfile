FROM gradle:7.1.0-jdk11-hotspot as builder
RUN gradle --version && java -version
WORKDIR /code
COPY build.gradle settings.gradle /code/
RUN gradle clean build --no-daemon > /dev/null 2>&1 || true
COPY ./ /code/
RUN gradle clean build --no-daemon

FROM adoptopenjdk/openjdk11:alpine
COPY --from=builder /code/build/libs/configuration-service-0.0.1-SNAPSHOT.jar /app/app.jar
WORKDIR /app
CMD ["java", "-jar", "app.jar"]